var express = require('express');
var router = express.Router();
var query  = require('./../module/mysql_pool');
var db  = require('./../module/db');

//事务操作函数
function _getNewSqlParamEntity(sql, params, callback) {
    if (callback) {
        return callback(null, {
            sql: sql,
            params: params
        });
    }
    return {
        sql: sql,
        params: params
    };
}
//获取当前时间
function CurentTime()
{
    var now = new Date();

    var year = now.getFullYear();       //年
    var month = now.getMonth() + 1;     //月
    var day = now.getDate();            //日

    var hh = now.getHours();            //时
    var mm = now.getMinutes();          //分

    var clock = year + "-";

    if(month < 10)
        clock += "0";

    clock += month + "-";

    if(day < 10)
        clock += "0";

    clock += day + " ";

    if(hh < 10)
        clock += "0";

    clock += hh + ":";
    if (mm < 10) clock += '0';
    clock += mm;
    return(clock);
}
//获取一个订单
router.post('/Repairorder',function (req,res,next) {
    let sql = 'select * FROM repairorder where orderid = ? and warrantyid = ?'
    query(sql,[req.body.orderid,req.body.warrantyid],function (err, result) {
        if(err){
            console.log('[SELECT ERROR] - ',err.message);
            res.json({
                code:0,
                msg:'网络错误，请稍后重试'
            });
            res.end();
            return;
        }
        if(result[0]){
            res.json({
                code:1,
                data:result
            });
            res.end();
        }else{
            res.json({
                code:0,
                data:'网络错误，请稍后重试2'
            });
            return res.end();
        }
    })
})
//获取未受理的订单总数
router.post('/OrderNotacceptedCunt',function (req,res,next) {
    let sql = 'SELECT COUNT(*) FROM repairorder where wxrId is NULL and schedule = 0 '
    var totalRecord
    var maxResult = 2
    query(sql,function (err, result) {
        if(err){
            console.log('[SELECT ERROR] - ',err.message);
            res.json({
                code:0,
                msg:'网络错误，请稍后重试'
            });
            res.end();
            return;
        }
        if(result){
            totalRecord = result[0]['COUNT(*)']
            var  totalPage = parseInt((totalRecord + maxResult -1) / maxResult)
            console.log(totalRecord)
            res.json({
                code:1,
                totalPage:totalPage
            });
            res.end();
        }else{
            res.json({
                code:0,
                data:'网络错误，请稍后重试2'
            });
            return res.end();
        }
    })
})

//获取未受理的订单
router.post('/OrderNotaccepted',function (req,res,next) {
    var start = (req.body.page - 1) * 2;
    let sql = 'select * from repairorder left join category on repairorder.class = category.id where wxrId is NULL and schedule = 0 order by orderid DESC limit ?,2;  '
    query(sql,[start],function (err, result) {
        if(err){
            console.log('[SELECT ERROR] - ',err.message);
            res.json({
                code:0,
                msg:'网络错误，请稍后重试'
            });
            res.end();
            return;
        }
        if(result[0]){
            res.json({
                code:1,
                data:result
            });
            res.end();
        }else{
            res.json({
                code:0,
                data:'网络错误，请稍后重试2'
            });
            return res.end();
        }
    })
})
//获取所有报修单
router.post('/studentListRepairorderCount',function (req,res,next) {
    let sql = 'select * FROM repairorder where warrantyid = ? order by orderid DESC '
    query(sql,[req.body.id],function (err, result) {
        if(err){
            console.log('[SELECT ERROR] - ',err.message);
            res.json({
                code:0,
                msg:'网络错误，请稍后重试'
            });
            res.end();
            return;
        }

        if(result[0]){
            res.json({
                code:1,
                data:result
            });
            res.end();
        }else{
               res.json({
                code:0,
                data:'网络错误，请稍后重试2'
            });
            return res.end();
        }
    })
});
//维修人员的保修单
router.post('/RepairmanListRepairorderCount',function (req,res,next) {

    let sql = 'select * FROM repairorder where wxrId = ? order by orderid DESC '
    query(sql,[req.body.id],function (err, result) {
        if(err){
            console.log('[SELECT ERROR] - ',err.message);
            res.json({
                code:0,
                msg:'网络错误，请稍后重试'
            });
            res.end();
            return;
        }

        if(result[0]){
            res.json({
                code:1,
                data:result
            });
            return res.end();
        }else{
            res.json({
                code:0,
                data:'网络错误，请稍后重试2'
            });
            return res.end();
        }
    })
})


//新增项目
router.post('/addOrder',function (req,res,next) {
    var sqlParamsEntity = [];
    var order = req.body.order
    var addtime = CurentTime()
    console.log(order);
    let sql = 'INSERT INTO repairorder (Maintitle, Maincontent,warrantyid,address,addtime,MainIMG,Remarks,bespokeTime,class) VALUES (?,?,?,?,?,?,?,?,?)';
    var param1 = [order.Maintitle,order.Maincontent,order.warrantyid,order.address,addtime,order.MainIMG,order.Remarks,order.bespokeTime,order.class];
    sqlParamsEntity.push(_getNewSqlParamEntity(sql, param1));
    var sql2 = 'update dormitory set count=count+1 where id = ?'
    var param2 = order.addressId
    sqlParamsEntity.push(_getNewSqlParamEntity(sql2, param2));

    db.execTrans(sqlParamsEntity,function (err,result) {
        if(err){
            console.log(err)
            res.json({
                code:0,
                msg:'操作失败，请稍后再试！'
            });
            return
        }
        console.log(result+'响应的操作')
        if(result){
            res.json({
                code:1,
                data:'报修成功'
            });
        }else{
            res.json({
                code:0,
                msg:'操作失败，请稍后再试！'
            });
        }
    })
})



//撤销报修单
router.post('/delectOrder',function (req,res,next) {
    let sql = 'DELETE FROM repairorder WHERE orderid = ? and schedule = 0 and wxrId IS NULL'
    query(sql,[req.body.id],function (err, result) {
        if(err){
            console.log('[SELECT ERROR] - ',err.message);
            res.json({
                code:0,
                msg:'网络错误，请稍后重试'
            });
            return;
        }
        if(result.affectedRows){
            res.json({
                code:1,
                data:'撤销成功'
            });
        }else{
            res.json({
                code:0,
                msg:'操作失败，请稍后再试！'
            });
        }
    })
})
//提交评论
router.post('/getOrderComment',function (req,res,next) {
    let sql = 'UPDATE repairorder SET comment = ?,score = ? WHERE orderid = ? and schedule = 2'
    query(sql,[req.body.comment,req.body.score,req.body.id],function (err, result) {
        if(err){
            console.log('[SELECT ERROR] - ',err.message);
            res.json({
                code:0,
                msg:'网络错误，请稍后重试'
            });
            return;
        }
        if(result.affectedRows){
            res.json({
                code:1,
                data:'评论成功'
            });
        }else{
            res.json({
                code:0,
                msg:'评论失败，请稍后再试！'
            });
        }
    })
})
//学生提交完成订单
router.post('/studentPostCompleted',function (req,res,next) {
    let sql = 'update repairorder set schedule= 2 where orderid = ? and warrantyid = ?'
    query(sql,[req.body.orderid,req.body.warrantyid],function (err,result) {
        if(err){
            console.log('[SELECT ERROR] - ',err.message);
            res.json({
                code:0,
                msg:'网络错误，请稍后重试'
            });
            return;
        }
        if(result.affectedRows){
            res.json({
                code:1,
                data:'提交成功'
            });
        }else{
            res.json({
                code:0,
                msg:'提交失败，请稍后再试！'
            });
        }
    })
})
//维修人员回复
router.post('/wxrPostReply',function(req,res,next) {
    let sql = 'UPDATE repairorder SET Reply = ? WHERE orderid = ? and schedule = 2'
    query(sql,[req.body.Reply,req.body.orderid],function (err,result) {
        if(err){
            console.log('[SELECT ERROR] - ',err.message);
            res.json({
                code:0,
                msg:'网络错误，请稍后重试'
            });
            return;
        }
        if(result.affectedRows){
            res.json({
                code:1,
                data:'提交成功'
            });
        }else{
            res.json({
                code:0,
                msg:'提交失败，请稍后再试！'
            });
        }
    })
})
//取消受理操作
router.post('/cancelAcceptance',function (req,res,next) {
    let sql = 'UPDATE repairorder SET CancelledId = wxrId,wxrId = null,schedule=0 WHERE orderid = ? and schedule= 1 and wxrId = ?'
    query(sql,[req.body.orderid,req.body.wxrId],function (err,result) {
        if(err){
            console.log('[SELECT ERROR] - ',err.message);
            res.json({
                code:0,
                msg:'网络错误，请稍后重试'
            });
            return;
        }
        if(result.affectedRows){
            res.json({
                code:1,
                data:'取消成功'
            });
        }else{
            res.json({
                code:0,
                msg:'取消失败，请稍后再试！'
            });
        }
    })
})
//完成订单
router.post('/PostCompleted',function (req,res,next) {
    let sql = 'update repairorder set schedule= 2 where orderid = ? and wxrId = ?'
    query(sql,[req.body.orderid,req.body.wxrId],function (err,result) {
        if(err){
            console.log('[SELECT ERROR] - ',err.message);
            res.json({
                code:0,
                msg:'网络错误，请稍后重试'
            });
            return;
        }
        if(result.affectedRows){
            res.json({
                code:1,
                data:'提交成功'
            });
        }else{
            res.json({
                code:0,
                msg:'提交失败，请稍后再试！'
            });
        }
    })
})



module.exports = router;
