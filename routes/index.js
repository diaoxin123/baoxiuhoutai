var express = require('express');
var router = express.Router();
var app = express()
var query  = require('./../module/mysql_pool');

// router.get('/', function(req, res, next) {
//     res.render('index', { title: 'Chat' });
// });
router.get('/exit',function (req,res,next) {
    var token = req.session.token;
    var expires = req.session.expires
    // 判断token是否存在
    if(token && expires){
        req.session.token = ''
        req.session.expires = ''
        res.json({
            code:1,
            data:{
                msg:'退出成功!请重新登录'
            }
        })
    }else{
        res.json({
            code:0,
            data:{
                msg:'你还没有登录。'
            }
        })
    }
})
//获取分类内别
router.post('/category',function (req,res,next) {
    let sql = 'SELECT * FROM category'
    query(sql,function (err,result) {
        if(err){
            console.log('[SELECT ERROR] - ',err.message);
            res.json({
                code:0,
                msg:'网络错误，请稍后重试'
            });
            res.end();
            return;
        }
        if(result[0]){
            res.json({
                code:1,
                data:result
            });
            res.end();
        }else{
            res.json({
                code:0,
                msg:'操作失败，请稍后再试！'
            });
            res.end();
        }
    })
})

module.exports = router;
