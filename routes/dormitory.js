var express = require('express');
var router = express.Router();
var query  = require('./../module/mysql_pool');
//获取所有信息
router.post('/list', function(req, res, next) {
    let sqlstr = 'SELECT * from dormitory'
    query(sqlstr,function (err,result) {
        if(err){
            console.log('[SELECT ERROR] - ',err.message);
            res.json({
                code:0,
                msg:'网络错误，请稍后重试'
            });
            res.end();
            return;
        }
        if(result[0]){
            res.json({
                code:1,
                data:result
            });
            res.end();
        }else{
            res.json({
                code:0,
                msg:'添加失败，请稍后再试！'
            })
            res.end();
        }
    })
});
//增加一条信息
router.post('/addDormitory',function (req,res,next) {
    let sqlstr = 'INSERT INTO dormitory (name,floor,room,creetetime,updatetime) VALUES (?,?,?,?,?)'
    let xgtime ='0000-00-00'
    let myData = new Date();
    let insertTime = myData.toLocaleDateString().split('/').join('-');
    console.log(req.body.params)
    query(sqlstr,[req.body.name,req.body.floor,req.body.room,insertTime,xgtime],function (err,result) {
        if(err){
            console.log('[SELECT ERROR] - ',err.message);
            res.json({
                code:0,
                msg:'网络错误，请稍后重试'
            });
            res.end();
            return;
        }
        if(result.affectedRows){
            res.json({
                code:1,
                data:'新增成功'
            });
            res.end();
        }else{
            res.json({
                code:0,
                msg:'添加失败，请稍后再试！'
            });
            res.end();
        }
    })
})

//删除一条信息
router.post('/deleteDormitory',function (req,res,next) {
    let sqlstr = 'DELETE FROM dormitory WHERE id= ?'
    query(sqlstr,[req.body.id],function (err,result) {
        if(err){
            console.log('[SELECT ERROR] - ',err.message);
            res.json({
                code:0,
                msg:'网络错误，请稍后重试'
            });
            res.end();
            return;
        }
        if(result.affectedRows){
            res.json({
                code:1,
                data:'操作成功'
            });
            res.end();
        }else{
            res.json({
                code:0,
                msg:'操作失败了，请稍后再试'
            });
            res.end();
        }
    })
})
//修改一条数据
router.post('/modify',function (req,res,next) {
    let sqlstr = 'UPDATE dormitory SET name = ?,floor = ?,room = ?,updatetime = ? WHERE id = ?'
    let myData = new Date();
    let updatetime = myData.toLocaleDateString().split('/').join('-');
    query(sqlstr,[req.body.name,req.body.floor,req.body.room,req.body.id,updatetime],function (err,result) {
        if(err){
            console.log('[SELECT ERROR] - ',err.message);
            res.json({
                code:0,
                msg:'网络错误，请稍后重试'
            });
            res.end();
            return;
        }
        console.log(result.affectedRows);
        if(result.affectedRows){
            res.json({
                code:1,
                data:'操作成功'
            });
            res.end();
        }else{
            res.json({
                code:0,
                msg:'操作失败了，请稍后再试'
            });
            res.end();
        }
    })
})
module.exports = router;
