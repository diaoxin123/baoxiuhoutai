var express = require('express');
var router = express.Router();
var app = express()
var query  = require('./../module/mysql_pool');



//学生列表
router.get('/',function (req,res,next) {
    let sqlstr = 'SELECT stuId,stuName,stuImgSrc,stuTel,addtime,stuCount,stuZhuangtai FROM stuInfo'
    query(sqlstr,function (err,result) {
        if(err){
            console.log('[SELECT ERROR] - ',err.message);
            res.json({
                code:0,
                msg:'网络错误，请稍后再试！'
            });
            return;
        }
        if(result[0]){
            res.json({
                code:1,
                data:result
            });
        }else{
            res.json({
                code:0,
                msg:'网络错误，请稍后再试！'
            });
        }
    })
})

router.post('/SelectStudent',function (req,res,next) {
    let sql = 'SELECT stuId,stuName,stuImgSrc,stuTel FROM stuInfo WHERE stuId = ?'
    query(sql,[req.body.id],function (err,result) {
        if(err){
            console.log('[SELECT ERROR] - ',err.message);
            res.json({
                code:0,
                msg:'网络错误，请稍后再试！'
            });
            return;
        }
        if(result[0]){
            res.json({
                code:1,
                data:result
            });
        }else{
            res.json({
                code:0,
                msg:'网络错误，请稍后再试！'
            });
        }
    })
})

//学生注册
router.post('/addStudent',function (req,res,next) {
    let sql = 'INSERT INTO stuInfo (stuId,stuName,stuPwd,stuImgSrc,stuTel,addtime) VALUES (?,?,?,?,?,?)';
    query(sql,[req.body.student.Uid,req.body.student.Name,req.body.student.pwd,req.body.student.PackImg,req.body.student.Tel,req.body.student.addtime],function (err,result) {
        if(err){
            console.log('[SELECT ERROR] - ',err.message);
            res.json({
                code:0,
                msg:'网络错误，请稍后再试！'
            });
            return;
        }
        if(result.affectedRows){
            res.json({
                code:1,
                data:'注册成功'
            });
        }else{
            res.json({
                code:0,
                msg:'注册失败，请稍后再试！'
            });
        }
    })
})



//状态更新
router.post('/getzhuangtai',function (req,res,next) {
    let sqlstr = 'UPDATE stuInfo SET stuZhuangtai = ? WHERE stuId = ? '

    query(sqlstr,[req.body.stuzhuangtai,req.body.id],function (err,result) {
        if(err){
            console.log('[SELECT ERROR] - ',err.message);
            res.json({
                code:0,
                msg:'网络错误，请稍后再试！'
            });
            return;
        }
        if(result.affectedRows){
            res.json({
                code:1,
                data:'修改成功'
            });
        }else{
            res.json({
                code:0,
                msg:'修改失败，请稍后再试！'
            });
        }
    })
})
//修改用户信息
router.post('/editStudent',function (req,res,next) {
    let sqlstr = 'UPDATE stuInfo SET stuName = ?,stuTel =?  WHERE stuId = ?'
    query(sqlstr,[req.body.student.stuName,req.body.student.stuTel,req.body.student.stuId],function (err,result) {
        if(err){
            console.log('[SELECT ERROR] - ',err.message);
            res.json({
                code:0,
                msg:'网络错误，请稍后再试！'
            });
            return;
        }
        if(result.affectedRows){
            res.json({
                code:1,
                data:'修改成功'
            });
        }else{
            res.json({
                code:0,
                msg:'修改失败，请稍后再试！'
            });
        }
    })
})



//删除维修学生用户
router.post('/deleteStudent',function (req,res,next) {
    let sqlstr = 'DELETE FROM stuInfo WHERE stuId = ?'
    query(sqlstr,[req.body.id],function (err,result) {
        if(err){
            console.log('[SELECT ERROR] - ',err.message);
            res.json({
                code:0,
                msg:'网络错误，请稍后再试！'
            });
            return;
        }
        if(result.affectedRows){
            res.json({
                code:1,
                data:'操作成功'
            });
        }else{
            res.json({
                code:0,
                msg:'操作失败，请稍后再试！'
            });
        }
    })
})

//检查用户名是否被占用
router.post('/inspectStu',function (req,res,next) {
    let sqlstr = 'SELECT stuId FROM stuInfo WHERE stuId = ?'
    query(sqlstr,[req.body.id],function (err,result) {
        if(err){
            console.log('[SELECT ERROR] - ',err.message);
            res.json({
                code:0,
                msg:'网络错误，请稍后再试！'
            });
            return;
        }
        if(result[0]){
            res.json({
                code:0,
                msg:'用户id已存在。'
            });
        }else{
            res.json({
                code:1
            });
        }
    })
})

module.exports = router;
