var express = require('express');
var router = express.Router();
var query  = require('./../module/mysql_pool');
/* GET users listing. */
router.get('/', function(req, res, next) {
    res.send('respond with a resource');
});
//修改密码
router.post('/modify', function(req, res, next) {

    let sqlstr = 'UPDATE adminUser SET userPwd = ? WHERE id = ?'
    query(sqlstr,[req.body.changPwd,req.body.id],function (err,result) {
        if(err){
            console.log('[SELECT ERROR] - ',err.message);
            res.json({
                code:0,
                msg:'修改失败，请稍后再试！'
            });
            return;
        }
        if(result.affectedRows){
            res.json({
                code:1,
                data:'修改成功'
            });
        }else{
            res.json({
                code:0,
                msg:'修改失败，请稍后再试！'
            });
        }
    })
});

router.get('/adminList',function (req,res,next) {
    let sqlstr = 'SELECT id,Nickname,qx,tel FROM adminUser'
    query(sqlstr,function (err,result) {
        if(err){
            console.log('[SELECT ERROR] - ',err.message);
            res.json({
                code:0,
                msg:'网络错误，请稍后重试'
            });
            return;
        }
        if(result[0]){
            res.json({
                code:1,
                data:result
            });
        }else{
            res.json({
                code:0,
                msg:'网络错误，请稍后重试！'
            });
        }
    })
})
//检车用户唯一行
router.post('/inspectId',function (req,res,next) {
    let  sqlstr = 'SELECT id FROM adminUser WHERE id= ?'
    query(sqlstr,[req.body.id],function (err,result) {
        if(err){
            console.log('[SELECT ERROR] - ',err.message);
            res.json({
                code:0,
                msg:'网络错误，请稍后重试'
            });
            return;
        }
        if(result[0]){
            res.json({
                code:0,
                msg:'用户id已存在。'
            });
        }else{
            res.json({
                code:1
            });
        }
    })
})
//新增管理
router.post('/addAdminUser',function (req,res,next) {
    let sqlstr = 'INSERT INTO adminUser (id,Nickname,userPwd,qx,tel) VALUES (?,?,?,?,?)'
    query(sqlstr,[req.body.id,req.body.Nickname,req.body.pwd,req.body.qx,req.body.tel],function (err,result) {
        if(err){
            console.log('[SELECT ERROR] - ',err.message);
            res.json({
                code:0,
                msg:'网络错误，请稍后重试'
            });
            return;
        }
        if(result.affectedRows){
            res.json({
                code:1,
                data:'新增成功'
            });
        }else{
            res.json({
                code:0,
                msg:'新增失败，请稍后再试！'
            });
        }
    })
})

router.post('/removeAdmin',function (req,res,next) {
    let sqlstr = 'DELETE FROM adminUser WHERE id = ?'
    query(sqlstr,[req.body.id],function (err,result) {
        if(err){
            console.log('[SELECT ERROR] - ',err.message);
            res.json({
                code:0,
                msg:'网络错误，请稍后重试'
            });
            return;
        }
        if(result.affectedRows){
            res.json({
                code:1,
                data:'删除成功'
            });
        }else{
            res.json({
                code:0,
                msg:'删除失败，请稍后再试！'
            });
        }
    })
})
//修改信息
router.post('/editAdmin',function (req,res,next) {
    let sqlstr = 'UPDATE adminUser SET Nickname=?,userPwd=?,qx=?,tel=? WHERE id = ?'
    query(sqlstr,[req.body.Nickname,req.body.pwd,req.body.qx,req.body.tel,req.body.id],function (err,result) {
        if(err){
            console.log('[SELECT ERROR] - ',err.message);
            res.json({
                code:0,
                msg:'网络错误，请稍后重试'
            });
            return;
        }
        if(result.affectedRows){
            res.json({
                code:1,
                data:'修改成功'
            });
        }else{
            res.json({
                code:0,
                msg:'修改失败，请稍后再试！'
            });
        }
    })
})


module.exports = router;
