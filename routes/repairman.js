var express = require('express');
var router = express.Router();
var query  = require('./../module/mysql_pool');

router.get('/',function (req,res,next) {
    let sqlstr = 'SELECT id,wxrzc,wxrname,wxrtel,wxrlike,wxrcoutn,zhuangtai,wxrimgsrc FROM repairman'
    query(sqlstr,function (err,result) {
        if(err){
            console.log('[SELECT ERROR] - ',err.message);
            res.json({
                code:0,
                msg:'网络错误，请稍后再试！'
            });
            return;
        }
        if(result[0]){
            res.json({
                code:1,
                data:result
            });
        }else{
            res.json({
                code:0,
                msg:'网络错误，请稍后再试！'
            });
        }
    })
})
//修改用户状态
router.post('/getzhuangtai',function (req,res,next) {
    let sqlstr = 'UPDATE repairman SET zhuangtai = ?  WHERE id = ? '
    query(sqlstr,[req.body.zhuangtai,req.body.id],function (err,result) {
        if(err){
            console.log('[SELECT ERROR] - ',err.message);
            res.json({
                code:0,
                msg:'网络错误，请稍后再试！'
            });
            return;
        }
        if(result.affectedRows){
            res.json({
                code:1,
                data:'修改成功'
            });
        }else{
            res.json({
                code:0,
                msg:'修改失败，请稍后再试！'
            });
        }
    })
})
//修改用户信息
router.post('/editMan',function (req,res,next) {
    let sqlstr = 'UPDATE repairman SET wxrname = ?,wxrtel =?  WHERE id = ? '
    query(sqlstr,[req.body.man.wxrname,req.body.man.wxrtel,req.body.man.id],function (err,result) {
        if(err){
            console.log('[SELECT ERROR] - ',err.message);
            res.json({
                code:0,
                msg:'网络错误，请稍后再试！'
            });
            return;
        }
        if(result.affectedRows){
            res.json({
                code:1,
                data:'修改成功'
            });
        }else{
            res.json({
                code:0,
                msg:'修改失败，请稍后再试！'
            });
        }
    })
})




//删除维修人员的路由
router.post('/deleteRepairman',function (req,res,next) {

    let sqlstr = 'DELETE FROM repairman WHERE id = ? and wxrzc = ?'
    query(sqlstr,[req.body.id,req.body.wxrzc],function (err,result) {
        if(err){
            console.log('[SELECT ERROR] - ',err.message);
            res.json({
                code:0,
                msg:'网络错误，请稍后再试！'
            });
            return;
        }
        if(result.affectedRows){
            res.json({
                code:1,
                data:'操作成功'
            });
        }else{
            res.json({
                code:0,
                msg:'操作失败，请稍后再试！'
            });
        }
    })
})
//查询维修人员
router.post('/SelectRepairman',function (req,res,next) {

    let sqlstr = 'SELECT id,wxrzc,wxrname,wxrtel,wxrlike,wxrcoutn,wxrimgsrc  FROM repairman WHERE id = ? '
    query(sqlstr,[req.body.id],function (err,result) {
        if(err){
            console.log('[SELECT ERROR] - ',err.message);
            res.json({
                code:0,
                msg:'网络错误，请稍后再试！'
            });
            return;
        }
        if(result[0]){
            res.json({
                code:1,
                data:result
            });
        }else{
            res.json({
                code:0,
                msg:'操作失败，请稍后再试！'
            });
        }
    })
})

module.exports = router;
