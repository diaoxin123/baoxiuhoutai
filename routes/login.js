var express = require('express');
var router = express.Router();

var query  = require('./../module/mysql_pool');
var app = express();

//验证登录
var jwt = require('jwt-simple');
var moment = require('moment');
app.set('jwtTokenSecret', 'DIAOXINTOKEN');

router.post('/',function (req,res,next) {
    var loginSql = 'SELECT id,Nickname,qx FROM adminUser where id = ? and userPwd = ?';
    if(req.body.id != '' && req.body.pwd != ''){
        query(loginSql,[req.body.id ,req.body.pwd],function (err, result) {
            if(err){
                console.log('[SELECT ERROR] - ',err.message);
                res.json({
                    code:0,
                    msg:'网络错误，请稍后重试'
                });
                return;
            }
            if(result[0]){
                //TOKEN 验证
                //token 过期时间
                var expires = moment().add('days', 7).valueOf();
                //token加密对象
                var token = jwt.encode({
                    iss: req.body.id,
                }, app.get('jwtTokenSecret'));

                 //TOKEN存session
                 req.session.token = token
                 req.session.expires = expires

                res.json({
                    code:1,
                    data:{
                        Nickname:result[0].Nickname,
                        id:result[0].id,
                        authority:result[0].qx
                    }
                });
            }else{
                return   res.json({
                    code:0,
                    data:'登录失败，检查用户名或密码是否正确'
                });
            }
        })
    }else{
        res.json({
            code:0,
            data:'用户名密码不能为空'
        });
    }
})

router.post('/UserLogin',function (req,res,next) {

    let Sql = 'SELECT stuId,stuName,stuImgSrc,stuTel,stuCount,stuZhuangtai FROM stuInfo where stuId = ? and stuPwd = ?'
    if(req.body.id != '' && req.body.pwd != ''){
        query(Sql,[req.body.id ,req.body.pwd],function (err, result) {
            if(err){
                console.log('[SELECT ERROR] - ',err.message);
                res.json({
                    code:0,
                    msg:'网络错误，请稍后重试'
                });
                res.end();
                return;
            }
            if(result[0]){
                //TOKEN 验证
                //token 过期时间
                var expires = moment().add(7,'days').valueOf();
                //token加密对象
                var token = jwt.encode({
                    iss: req.body.id
                }, app.get('jwtTokenSecret'));
                console.log(token);
                req.session.token = token
                req.session.expires = expires

                res.json({
                    code:1,
                    data:result
                });
                res.end();
            }else{
                 res.json({
                    code:0,
                    data:'登录失败，检查用户名或密码是否正确'
                });
                return  res.end();
            }
        })
    }else{
        res.json({
            code:0,
            data:'用户名密码不能为空'
        });
    }
})



router.post('/WxrLogin',function (req,res,next) {
    let Sql = 'SELECT id,wxrzc,wxrname,wxrtel,wxrlike,wxrcoutn,zhuangtai,wxrimgsrc FROM repairman where wxrzc = ? and wxrpwd = ?'
    if(req.body.id != '' && req.body.pwd != ''){
        query(Sql,[req.body.id ,req.body.pwd],function (err, result) {
            if(err){
                console.log('[SELECT ERROR] - ',err.message);
                res.json({
                    code:0,
                    msg:'网络错误，请稍后重试'
                });
                return;
            }

            if(result[0]){
                //TOKEN 验证
                //token 过期时间
                var expires = moment().add('days', 7).valueOf();
                //token加密对象
                var token = jwt.encode({
                    iss: req.body.id,
                }, app.get('jwtTokenSecret'));
                req.session.token = token
                req.session.expires = expires

                res.json(200,{
                    code:1,
                    data:result
                });
            }else{
                return   res.json({
                    code:0,
                    data:'登录失败，检查用户名或密码是否正确'
                });
            }
        })
    }else{
        res.json({
            code:0,
            data:'用户名密码不能为空'
        });
    }
})

module.exports = router;
