var createError = require('http-errors');
var express = require('express');
var session = require('express-session');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var jwt = require('jwt-simple');

var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');
var loginRouter = require('./routes/login');
var dormitoryRouter = require('./routes/dormitory');
var repairmanRouter = require('./routes/repairman');
var studentRouter = require('./routes/student');
var repairorderRouter = require('./routes/repairorder');
var app = express();

// 跨域解决
var cors = require('cors')
app.use(cors({credentials: true, origin: 'http://localhost:8080'}));


app.set('jwtTokenSecret', 'DIAOXINTOKEN');

app.use(session({
    secret :  'diaoxin', // 对session id 相关的cookie 进行签名
    resave : true,
    saveUninitialized: false, // 是否保存未初始化的会话
    cookie : {
        maxAge : 1000 * 60 * 60 * 24 * 7, // 设置 session 的有效时间，单位毫秒
    }
}));


app.post('/*',function (req,res,next) {

    if(req.url=='/'|| req.url == '/login'|| req.url == '/login/UserLogin' || req.url == '/login/WxrLogin' || req.url == '/student/inspectStu' || req.url == '/student/addStudent'){
        return next();
    }
    var token = req.session.token
    var expires = req.session.expires
    // 判断token是否存在
    if (token) {
        try {
            var decoded = jwt.decode(token, app.get('jwtTokenSecret'));
            //验证token超时
            if (expires <= Date.now()) {
                    return  res.json({code:0,msg:'登录超时'});
                 }
            return next();
        } catch (err) {
            return  res.json({code:0,msg:'操作异常'});
        }
    } else {
        res.json({code:0,msg:'你还没有登陆'});
    }
});




// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', indexRouter);
app.use('/users', usersRouter);
app.use('/login', loginRouter);
app.use('/dormitory', dormitoryRouter);
app.use('/repairman',repairmanRouter);
app.use('/student',studentRouter);
app.use('/repairorder',repairorderRouter)

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
